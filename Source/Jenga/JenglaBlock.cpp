// Fill out your copyright notice in the Description page of Project Settings.

#include "JenglaBlock.h"

#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Engine/World.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "Engine/Classes/Components/BoxComponent.h"

#include "JengaGameMode.h"
#include "JengaTower.h"

#define BLOCK_TAG TEXT("block")

// Sets default values
AJenglaBlock::AJenglaBlock()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	blockMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BlockMesh"));
	//blockMesh->SetSimulatePhysics(true);
	RootComponent = blockMesh;

	collisionComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("BlockCollirder"));
	collisionComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);

	//collisionComponent->OnComponentHit.AddDynamic(this, &AJenglaBlock::OnHitEvent);
	//collisionComponent->OnClicked.AddDynamic(this, &AJenglaBlock::OnClickedEvent);

	collisionComponent->ComponentTags.Add(BLOCK_TAG);

	Hold(false);
}



// Called when the game starts or when spawned
void AJenglaBlock::BeginPlay()
{
	Super::BeginPlay();

}


void AJenglaBlock::OnClickedEvent(UPrimitiveComponent* TouchedComponent, FKey ButtonPressed)
{
	UE_LOG(LogTemp, Warning, TEXT("AJenglaBlock TouchedActor %s"), *TouchedComponent->GetName());
}

void AJenglaBlock::OnBeginOverlapEvent(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	AJengaGameMode* gameMode = Cast<AJengaGameMode>(GetWorld()->GetAuthGameMode());
	UE_LOG(LogTemp, Warning, TEXT("BeginOverlap %s %s"), *GetName(), *OtherComp->GetName());
	if (OtherComp->ComponentTags.Contains("floor"))
	{
		gameMode->GameOver();
		return;
	}
	else if (OtherComp->ComponentTags.Contains("block"))
	{
		Tags.Remove("dropped");
		gameMode->NextTurn();
	}

	collisionComponent->OnComponentBeginOverlap.RemoveDynamic(this, &AJenglaBlock::OnBeginOverlapEvent);
}

void AJenglaBlock::OnHitEvent(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	UE_LOG(LogTemp, Warning, TEXT("OnHitEvent %s"), *GetName());
}

// Called every frame
void AJenglaBlock::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AJenglaBlock::SetSelected(bool value)
{
	blockMesh->SetRenderCustomDepth(value);
	SetOnFloor(!value);
}

void AJenglaBlock::Hold(bool value)
{
	if (value)
	{
		UE_LOG(LogTemp, Warning, TEXT("HOLD %s"), *GetName());
		collisionComponent->OnComponentBeginOverlap.AddDynamic(this, &AJenglaBlock::OnBeginOverlapEvent);
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("DROP %s"), *GetName());
		Tags.Add("dropped");
	}
	SetSelected(value);
	blockMesh->SetSimulatePhysics(!value);
}

void AJenglaBlock::Move(FVector force)
{
	SetActorLocation(GetActorLocation() + force);
}

void AJenglaBlock::SetOnFloor(bool enabled)
{
	if (enabled)
	{
		collisionComponent->ComponentTags.Remove("blockOnFloor");
	}
	else
	{
		collisionComponent->ComponentTags.Add("blockOnFloor");
	}
}

void AJenglaBlock::SetSimulatePhysics(bool enabled)
{
	blockMesh->SetSimulatePhysics(enabled);
}

