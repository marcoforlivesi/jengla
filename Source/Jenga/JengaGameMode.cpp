// Fill out your copyright notice in the Description page of Project Settings.

#include "JengaGameMode.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"

#include "JengaHUD.h"
#include "Jenga/JengaPawn.h"
#include "Jenga/JengaTower.h"

AJengaGameMode::AJengaGameMode()
{
}


void AJengaGameMode::BeginPlay()
{
	Super::BeginPlay();

	for (int i = 0; i < numPlayers; i++)
	{
		APlayerController* playerController = UGameplayStatics::CreatePlayer(this, i, true);
		if (!playerController)
		{
			playerController = UGameplayStatics::GetPlayerController(this, i);
		}
		players.Add(Cast<AJengaPawn>(playerController->GetOwner()));
	}

	currentPlayer = UGameplayStatics::GetPlayerController(this, 0);
}

void AJengaGameMode::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

const UJengaTower * AJengaGameMode::GetTower() const
{
	return tower;
}

void AJengaGameMode::SetTower(UJengaTower * tower)
{
	this->tower = tower;
}

void AJengaGameMode::NextTurn()
{
	if (currentPlayer == nullptr)
	{
		return;
	}

	uint32 id = currentPlayer->GetUniqueID();
	currentPlayer = UGameplayStatics::GetPlayerController(this, (id + 1) % numPlayers);
	tower->SaveState();
	++numTurns;

	UE_LOG(LogTemp, Warning, TEXT("Next Turn"));
}

void AJengaGameMode::GameOver()
{
	AJengaHUD* hud = Cast<AJengaHUD>(UGameplayStatics::GetPlayerController(this, 0)->GetHUD());
	hud->ShowGameOver();
}

int AJengaGameMode::GetNumTurns()
{
	return numTurns;
}

void AJengaGameMode::Undo()
{
	tower->Undo();
	numTurns--;
}

void AJengaGameMode::Redo()
{
	tower->Redo();
	numTurns++;
}

