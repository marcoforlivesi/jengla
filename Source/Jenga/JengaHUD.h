// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "JengaHUD.generated.h"

/**
 * 
 */
UCLASS()
class JENGA_API AJengaHUD : public AHUD
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintImplementableEvent, Category = TDLSurvivorCache)
		void ShowGameOver();
	
protected:
};
