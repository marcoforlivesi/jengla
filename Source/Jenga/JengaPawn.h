// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "JengaPawn.generated.h"

class AJenglaBlock;
class UJengaTower;
class USpringArmComponent;
class UCameraComponent;

enum class SelectionState {
	DISABLED,
	SELECTED,
	HOLD,
};

UCLASS()
class JENGA_API AJengaPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AJengaPawn();

	const UJengaTower* GetTower() const;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	AJenglaBlock* GetSelectedBlock() const;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
	void OnClickedEvent(AActor* TouchedActor, FKey ButtonPressed);
	UFUNCTION()
	void OnBeginCursorOverEvent(AActor* TouchedActor);
	UFUNCTION()
	void OnEndCursorOverEvent(AActor* TouchedActor);

	SelectionState GetState() const;
	void SetState(const SelectionState& blockSate);
	bool IsHold() const;
	bool IsDisabled() const;
	
	void SelectBlock(AJenglaBlock* block);
	AJenglaBlock* UpdateSelection();
	void OnSelectBlock();
	void MoveForward(float Val);
	void MoveRight(float Val);
	void MoveBlockForward(float AxisValue);
	void MoveBlockRight(float AxisValue);
	void MoveBlockUp(float AxisValue);
	void YawBlock(float AxisValue);
	void FollowBlock(AJenglaBlock* block);

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Jengla")
	float peekDistance = 500.0f;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Jengla")
	float force = 2.0f;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Jengla")
	USpringArmComponent* SpringArm;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Jengla")
	UCameraComponent* Camera;

	FComponentQueryParams DefaultComponentQueryParams;
	FCollisionResponseParams DefaultResponseParam;

	AJenglaBlock* currentSelection;
	SelectionState blockState;
	
};
