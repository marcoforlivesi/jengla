// Fill out your copyright notice in the Description page of Project Settings.

#include "JengaTower.h"

#include "Engine/World.h"

#include "JengaGameMode.h"
#include "JenglaBlock.h"

// Sets default values for this component's properties
UJengaTower::UJengaTower()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}


// Called when the game starts
void UJengaTower::BeginPlay()
{
	Super::BeginPlay();

	CreateTower();

	SaveState();
}


// Called every frame
void UJengaTower::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	
}

const TArray<AJenglaBlock*> UJengaTower::GetBlocks() const
{
	return blocks;
}

void UJengaTower::CreateTower()
{
	AJengaGameMode* gameMode = Cast<AJengaGameMode>(GetWorld()->GetAuthGameMode());
	gameMode->SetTower(this);
	FVector gridPosition = this->GetOwner()->GetActorLocation();

	// ...
	for (int floor = 0; floor < floors; floor++)
	{
		for (int row = 0; row < rows; row++)
		{
			AJenglaBlock* block = GetWorld()->SpawnActor<AJenglaBlock>(blockBP);

			bool rotate = floor % 2;

			FVector blockScale = block->GetActorScale();
			int posX = row - rows / 2;
			block->SetActorLocation(gridPosition +
				FVector(!rotate * posX * blockScale.X * scaleFactor,
					rotate *  posX * blockScale.X * scaleFactor,
					floor * blockScale.Z * scaleFactor));
			block->SetActorRotation(block->GetActorRotation().Add(0, rotate * 90.0f, 0));

			blocks.Add(block);
		}
	}
}

void UJengaTower::SaveState()
{
	JengaTowerState state;

	for (AJenglaBlock* block : blocks)
	{
		state.Add(block->GetActorTransform());
	}

	undo.Add(state);
}

void UJengaTower::Undo()
{
	UE_LOG(LogTemp, Warning, TEXT("Undo"));
	if (undo.Num() == 0)
	{
		return;
	}

	const JengaTowerState state = undo.Pop(true);

	for (int i = 0; i < blocks.Num(); i++)
	{
		blocks[i]->SetActorTransform(state[i]);
	}

	redo.Add(state);
}

void UJengaTower::Redo()
{
	UE_LOG(LogTemp, Warning, TEXT("Redo"));
	if (redo.Num() == 0)
	{
		return;
	}

	const JengaTowerState state = redo.Pop(true);

	for (int i = 0; i < blocks.Num(); i++)
	{
		blocks[i]->SetActorTransform(state[i]);
	}

	undo.Add(state);
}

