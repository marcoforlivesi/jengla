// Fill out your copyright notice in the Description page of Project Settings.

#include "JengaPawn.h"

#include "Engine/World.h"
#include "Engine/Classes/Components/PrimitiveComponent.h"
#include "Engine/Classes/Components/BoxComponent.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "DrawDebugHelpers.h"

#include "JengaGameMode.h"
#include "JengaTower.h"
#include "JenglaBlock.h"


inline void ShowEdge(AJenglaBlock* currentBlock, bool show)
{
	if (currentBlock == nullptr)
	{
		return;
	}

	currentBlock->SetSelected(show);
}

// Sets default values
AJengaPawn::AJengaPawn()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Set this pawn to be controlled by the lowest-numbered player
	AutoPossessPlayer = EAutoReceiveInput::Player0;

	// Create a dummy root component we can attach things to.
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));

	SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm"));
	SpringArm->TargetArmLength = 100.0f;
	SpringArm->bEnableCameraLag = true;
	SpringArm->CameraLagSpeed = 2.0f;
	SpringArm->bEnableCameraRotationLag = false;
	SpringArm->bUsePawnControlRotation = false;
	SpringArm->bDoCollisionTest = false;
	SpringArm->SetWorldRotation(FRotator(-0, 0.0f, 0.0f));

	// Create a camera and a visible object
	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("OurCamera"));
	// Attach our camera and visible object to our root component. Offset and rotate the camera.
	Camera->SetupAttachment(RootComponent);
	Camera->SetRelativeLocation(FVector(-100.0f, 0.0f, 250.0f));
	//Camera->SetRelativeRotation(FRotator(-15.0f, 0.0f, 0.0f));

	currentSelection = nullptr;
	blockState = SelectionState::DISABLED;

	OnClicked.AddDynamic(this, &AJengaPawn::OnClickedEvent);
	OnBeginCursorOver.AddDynamic(this, &AJengaPawn::OnBeginCursorOverEvent);
	OnEndCursorOver.AddDynamic(this, &AJengaPawn::OnEndCursorOverEvent);
}

// Called when the game starts or when spawned
void AJengaPawn::BeginPlay()
{
	Super::BeginPlay();

}

const UJengaTower* AJengaPawn::GetTower() const
{
	AJengaGameMode* gameMode = Cast<AJengaGameMode>(GetWorld()->GetAuthGameMode());
	return gameMode->GetTower();
}

// Called every frame
void AJengaPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);


	AJenglaBlock* currentItem = UpdateSelection();
	if (GetSelectedBlock() == nullptr)
	{
		//SelectBlock(currentItem);
	}
}


void AJengaPawn::OnClickedEvent(AActor *TouchedActor, FKey ButtonPressed)
{
	UE_LOG(LogTemp, Warning, TEXT("TouchedActor %s"), *TouchedActor->GetName());
}

SelectionState AJengaPawn::GetState() const
{
	return blockState;
}

void AJengaPawn::SetState(const SelectionState& blockSate)
{
	this->blockState = blockSate;
}

bool AJengaPawn::IsHold() const
{
	return blockState == SelectionState::HOLD;
}

bool AJengaPawn::IsDisabled() const
{
	return blockState == SelectionState::DISABLED;
}

void AJengaPawn::SelectBlock(AJenglaBlock* currentItem)
{
	TArray<AJenglaBlock*> blocks = GetTower()->GetBlocks();

	if (currentItem && blockState == SelectionState::SELECTED)
	{
		blockState = SelectionState::HOLD;
		
		for (AJenglaBlock* block : blocks)
		{
			block->SetSimulatePhysics(true);
		}
		currentSelection->Hold(true);

		
		//FollowBlock(currentSelection);

		UE_LOG(LogTemp, Warning, TEXT("Item %s selected %s"), *currentSelection->GetName(), *FString::FromInt(IsHold()));
	}
	else
	{
		blockState = SelectionState::DISABLED;
		if (currentSelection) {
			currentSelection->Hold(false);
			UE_LOG(LogTemp, Warning, TEXT("Item %s dropped %s"), *currentSelection->GetName(), *FString::FromInt(IsHold()));
		}
		for (AJenglaBlock* block : blocks)
		{
			block->SetSimulatePhysics(false);
		}
		//FollowBlock(nullptr);

		ShowEdge(currentSelection, false);
		currentSelection = nullptr;
	}
}

AJenglaBlock* AJengaPawn::UpdateSelection()
{
	if (IsHold())
	{
		return currentSelection;
	}

	APlayerController* playerController = GetWorld()->GetFirstPlayerController();
	FVector mouseLocation, mouseDirection;
	playerController->DeprojectMousePositionToWorld(mouseLocation, mouseDirection);
	FVector start = mouseLocation;
	FVector direction = mouseDirection;
	FVector end = ((direction * peekDistance) + start);

	//DrawDebugLine(GetWorld(), start, end, FColor::Green, false, 1, 0, 1);

	FHitResult hit;

	if (GetWorld()->LineTraceSingleByChannel(hit, start, end, ECC_Visibility, DefaultComponentQueryParams, DefaultResponseParam))
	{
		if (hit.GetActor()->GetClass()->IsChildOf(AJenglaBlock::StaticClass()))
		{
			AJenglaBlock* currentItem = Cast<AJenglaBlock>(hit.GetActor());

			ShowEdge(currentSelection, false);
			currentSelection = currentItem;
			blockState = SelectionState::SELECTED;
			ShowEdge(currentSelection, true);

			return currentItem;
		}
	}
	
	return nullptr;
}

void AJengaPawn::OnSelectBlock()
{
	SelectBlock(currentSelection);
}

// Called to bind functionality to input
void AJengaPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// Respond every frame to the values of our two movement axes, "MoveX" and "MoveY".
	InputComponent->BindAxis("MoveForward", this, &AJengaPawn::MoveForward);
	InputComponent->BindAxis("MoveRight", this, &AJengaPawn::MoveRight);

	InputComponent->BindAxis("Turn", this, &AJengaPawn::AddControllerYawInput);
	InputComponent->BindAxis("LookUp", this, &AJengaPawn::AddControllerPitchInput);

	InputComponent->BindAction("SelectBlock", IE_Pressed, this, &AJengaPawn::OnSelectBlock);
	//InputComponent->BindAction("SelectBlock", IE_Released, this, &AJengaPawn::OnSelectBlock);

	InputComponent->BindAxis("MoveBlockUp", this, &AJengaPawn::MoveBlockUp);
	InputComponent->BindAxis("YawBlock", this, &AJengaPawn::YawBlock);

}

void AJengaPawn::OnBeginCursorOverEvent(AActor* TouchedActor)
{
	UE_LOG(LogTemp, Warning, TEXT("TouchedComponentBegin %s"), *TouchedActor->GetName());
}

void AJengaPawn::OnEndCursorOverEvent(AActor* TouchedActor)
{
	UE_LOG(LogTemp, Warning, TEXT("TouchedComponentEnd %s"), *TouchedActor->GetName());
}

AJenglaBlock* AJengaPawn::GetSelectedBlock() const
{
	if (!IsHold())
	{
		return nullptr;
	}
	return currentSelection;
}

void AJengaPawn::MoveForward(float Val)
{
	if (Val != 0.f)
	{
		if (IsHold())
		{
			MoveBlockForward(Val);
		}

			if (Controller)
			{
				FRotator const ControlSpaceRot = Controller->GetControlRotation();

				// transform to world space and add it
				AddMovementInput(FRotationMatrix(ControlSpaceRot).GetScaledAxis(EAxis::X), Val);
			}
		
	}
}

void AJengaPawn::MoveRight(float Val)
{
	if (Val != 0.f)
	{
		if (IsHold())
		{
			MoveBlockRight(Val);
		}

			if (Controller)
			{
				FRotator const ControlSpaceRot = Controller->GetControlRotation();

				// transform to world space and add it
				AddMovementInput(FRotationMatrix(ControlSpaceRot).GetScaledAxis(EAxis::Y), Val);
			}

	}
}

void AJengaPawn::MoveBlockForward(float AxisValue)
{
	if (IsHold())
	{
		currentSelection->Move(GetActorForwardVector() * force * AxisValue);
		//currentSelection->Move(currentSelection->GetActorRightVector() * force * AxisValue);
	}
}

void AJengaPawn::MoveBlockRight(float AxisValue)
{
	if (IsHold())
	{
		currentSelection->Move(GetActorRightVector() * force * AxisValue);
		//currentSelection->Move(currentSelection->GetActorForwardVector() * force * AxisValue);
	}
}

void AJengaPawn::MoveBlockUp(float AxisValue)
{
	if (IsHold())
	{
		currentSelection->Move(currentSelection->GetActorUpVector() * force * AxisValue);
	}

	if (Controller)
	{
		FRotator const ControlSpaceRot = Controller->GetControlRotation();

		// transform to world space and add it
		AddMovementInput(FRotationMatrix(ControlSpaceRot).GetScaledAxis(EAxis::Z), AxisValue);
	}
}

void AJengaPawn::YawBlock(float AxisValue)
{
	if (IsHold())
	{
		currentSelection->GetActorRotation().Add(0, 0, force * AxisValue);
	}
}

void AJengaPawn::FollowBlock(AJenglaBlock * block)
{
	if (block)
	{
		SpringArm->AttachToComponent(currentSelection->GetRootComponent(), FAttachmentTransformRules::KeepRelativeTransform);
		Camera->AttachToComponent(SpringArm, FAttachmentTransformRules::KeepRelativeTransform);
	}
}
