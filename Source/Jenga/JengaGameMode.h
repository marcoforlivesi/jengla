// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "JengaGameMode.generated.h"

class AJengaPawn;
class UJengaTower;

UENUM()
enum class EGamePlayState
{
	EPlaying,
	EGameOver,
	EUnknown
}; 

/**
 * 
 */
UCLASS()
class JENGA_API AJengaGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AJengaGameMode();

	virtual void BeginPlay() override;

	virtual void Tick(float DeltaTime) override;

	const UJengaTower* GetTower() const;
	void SetTower(UJengaTower* tower);

	void NextTurn();
	void GameOver();

	UFUNCTION(BlueprintCallable, Category = "Jenga")
	int GetNumTurns();
	UFUNCTION(BlueprintCallable, Category = "Jenga")
	void Undo();
	UFUNCTION(BlueprintCallable, Category = "Jenga")
	void Redo();
protected:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Jengla")
	int numPlayers = 3;

	TArray<AJengaPawn*> players;
	APlayerController* currentPlayer;
	UJengaTower* tower;
	int numTurns = 0;

};
