// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "JenglaBlock.generated.h"

class UBoxComponent;

UCLASS()
class JENGA_API AJenglaBlock : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AJenglaBlock();

	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	void SetSelected(bool value);
	void Hold(bool value);
	void Move(FVector force);
	void SetOnFloor(bool enabled);
	void SetSimulatePhysics(bool enabled);
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	//DECLARE_DYNAMIC_MULTICAST_DELEGATE_FourParams(FComponentEndOverlapSignature, UPrimitiveComponent*, OverlappedComponent, AActor*, OtherActor, UPrimitiveComponent*, OtherComp, int32, OtherBodyIndex);
	//DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FComponentBeginCursorOverSignature, UPrimitiveComponent*, TouchedComponent);
	//DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FComponentEndCursorOverSignature, UPrimitiveComponent*, TouchedComponent);
	//DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FComponentOnReleasedSignature, UPrimitiveComponent*, TouchedComponent, FKey, ButtonReleased);

	UFUNCTION()
	void OnBeginOverlapEvent(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
	void OnClickedEvent(UPrimitiveComponent* TouchedComponent, FKey ButtonPressed);
	UFUNCTION()
	void OnHitEvent(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Jengla")
	UStaticMeshComponent* blockMesh;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Jengla")
	UBoxComponent* collisionComponent;

	
};
