// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "JengaTower.generated.h"

class AJenglaBlock;
typedef TArray<FTransform> JengaTowerState;

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class JENGA_API UJengaTower : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UJengaTower();

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	const TArray<AJenglaBlock*> GetBlocks() const;

	void CreateTower();

	void SaveState();
	void Undo();
	void Redo();
protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Jengla")
	TSubclassOf<class AJenglaBlock> blockBP;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Jengla")
	int floors = 7;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Jengla")
	int rows = 3;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Jengla")
	float scaleFactor = 100.0f;
private:
	TArray<AJenglaBlock*> blocks;
	TArray<JengaTowerState> undo;
	TArray<JengaTowerState> redo;
};
